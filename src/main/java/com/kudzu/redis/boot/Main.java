package com.kudzu.redis.boot;

import com.kudzu.redis.core.RedisFactory;
import com.kudzu.redis.core.RedisSetting;
import com.xdxiantianyi.bean.XtAppUserDO;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanResult;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by kudzu-notebook-hp on 2018/2/3.
 *
 * @since 1.0
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        if (args == null || args.length == 0) {
            System.out.println("请输入手机号,形如: java -jar xx.jar 13140199395");
            return;
        }
        boolean debug = args.length >= 2 ? "debug".equals(args[1]) : false;
        String mobile = args[0];
        RedisSetting redisSetting = new RedisSetting();
        redisSetting.setMaxTatol(10);
        redisSetting.setPort(6379);
        redisSetting.setHost("localhost");
        redisSetting.setPassword("1q2w3e4r5t");
        // redisSetting.setHost("47.100.13.140");
        // redisSetting.setPassword("myRedis");
        redisSetting.setDatabase(1);
        redisSetting.setTimeout(3000);

        System.out.format("redis信息：host:%s,password:%s,port:%s,database:%s", redisSetting.getHost(),
                redisSetting.getPassword(), redisSetting.getPort(), redisSetting.getDatabase());
        System.out.println();

        RedisFactory.init(redisSetting);

        int count = 0;
        int total = 0;

        try (Jedis jedis = RedisFactory.getJedis()) {
            String cursor = "0";

            do {
                ScanResult<String> result = jedis.scan(cursor);
                cursor = result.getStringCursor();
                for (String key : result.getResult()) {
                    total++;
                    byte[] bytes = jedis.get(key.getBytes());
                    try {
                        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
                        Object obj = ois.readObject();
                        if (obj instanceof XtAppUserDO) {
                            XtAppUserDO xtAppUserDO = (XtAppUserDO) obj;
                            String userLogin = xtAppUserDO.getUserLogin();
                            if (mobile.equals(userLogin)) {
                                long status = jedis.del(key);
                                if (status > 0) {
                                    count++;
                                    System.out.println("找到一个游客:" + key + "已删除");
                                }
                            } else {
                                if (debug) {
                                    System.out.println("用户:" + userLogin + " 不是游客不执行");
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println(">>读取对象IO异常");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        System.out.println(">>读取对象未找到");
                    }
                }
            } while (!"0".equals(cursor));
        }
        System.out.println("查找完毕宫遍历keys:" + total + "，共找到：" + count + "个，已全部删除");
        //Thread.sleep(100000000000L);
    }
}
