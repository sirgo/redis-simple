package com.kudzu.redis.core;

/**
 * Created by kudzu-notebook-hp on 2018/2/3.
 *
 * @since 1.0
 */
public class RedisSetting {

    private String host;

    private String password;

    private int database;

    private int port;

    private int maxTatol;

    private int timeout;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getMaxTatol() {
        return maxTatol;
    }

    public void setMaxTatol(int maxTatol) {
        this.maxTatol = maxTatol;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
}
