package com.kudzu.redis.core;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by kudzu-notebook-hp on 2018/2/3.
 *
 * @since 1.0
 */
public class RedisFactory {
    private static JedisPool jedisPool = null;

    public static void init(RedisSetting setting) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(setting.getMaxTatol());
        jedisPool = new JedisPool(jedisPoolConfig, setting.getHost(), setting.getPort(), setting.getTimeout(),
                setting.getPassword(), setting.getDatabase());

    }

    public static Jedis getJedis() {
        return jedisPool.getResource();
    }

}
