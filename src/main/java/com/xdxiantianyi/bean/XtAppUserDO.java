package com.xdxiantianyi.bean;

import java.io.Serializable;
import java.util.Date;

public class XtAppUserDO implements Serializable {

    private static final long serialVersionUID = -931677836710407662L;

    private Long id;

    private String userLogin;

    private String userPass;

    private String userNickname;

    private String userEmail;

    private String userUrl;

    private Date userRegistered;

    private Date userUpdated;

    private Date userLastLogin;

    private Date userBirth;

    private Integer userStatus;

    private String surname;

    private String name;

    private String userToken;

    private Long userBindId;

    private String userGender;

    private Integer userLevel;

    private String recommender;

    private Long recommenderUserid;

    private Long recommenderUseridLevel2;

    private Long recommenderUseridLevel3;

    private Integer score;

    private Integer scoreCheckInTimes;

    private Double rewardMoney;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin == null ? null : userLogin.trim();
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass == null ? null : userPass.trim();
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname == null ? null : userNickname.trim();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl == null ? null : userUrl.trim();
    }

    public Date getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(Date userRegistered) {
        this.userRegistered = userRegistered;
    }

    public Date getUserUpdated() {
        return userUpdated;
    }

    public void setUserUpdated(Date userUpdated) {
        this.userUpdated = userUpdated;
    }

    public Date getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(Date userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    public Date getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(Date userBirth) {
        this.userBirth = userBirth;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname == null ? null : surname.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken == null ? null : userToken.trim();
    }

    public Long getUserBindId() {
        return userBindId;
    }

    public void setUserBindId(Long userBindId) {
        this.userBindId = userBindId;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender == null ? null : userGender.trim();
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public String getRecommender() {
        return recommender;
    }

    public void setRecommender(String recommender) {
        this.recommender = recommender == null ? null : recommender.trim();
    }

    public Integer getScore() {
        if (score == null) {
            score = new Integer(0);
        }
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScoreCheckInTimes() {
        if (scoreCheckInTimes == null) {
            return null;
        }
        return scoreCheckInTimes;
    }

    public void setScoreCheckInTimes(Integer scoreCheckInTimes) {
        this.scoreCheckInTimes = scoreCheckInTimes;
    }

    public Long getRecommenderUserid() {
        return recommenderUserid;
    }

    public void setRecommenderUserid(Long recommenderUserid) {
        this.recommenderUserid = recommenderUserid;
    }

    public Long getRecommenderUseridLevel2() {
        return recommenderUseridLevel2;
    }

    public void setRecommenderUseridLevel2(Long recommenderUseridLevel2) {
        this.recommenderUseridLevel2 = recommenderUseridLevel2;
    }

    public Long getRecommenderUseridLevel3() {
        return recommenderUseridLevel3;
    }

    public void setRecommenderUseridLevel3(Long recommenderUseridLevel3) {
        this.recommenderUseridLevel3 = recommenderUseridLevel3;
    }

    public Double getRewardMoney() {
        return rewardMoney;
    }

    public void setRewardMoney(Double rewardMoney) {
        this.rewardMoney = rewardMoney;
    }
}